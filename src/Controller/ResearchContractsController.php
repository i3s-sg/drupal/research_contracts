<?php

namespace Drupal\research_contracts\Controller;

use Drupal\Core\Controller\ControllerBase;
use Drupal\Core\Template\TwigEnvironment;
use Drupal\research_contracts\Service\ResearchContractsService;
use Symfony\Component\DependencyInjection\ContainerInterface;

class ResearchContractsController extends ControllerBase
{
    private $contractsService;
    private $modulePath;
    private $twig;

    public function __construct(
        ResearchContractsService $contractsService,
        TwigEnvironment $twig
    ) {

        $this->twig = $twig;
        $this->contractsService = $contractsService;
        $this->modulePath = drupal_get_path('module', 'research_contracts');
    }

    public static function create(ContainerInterface $container)
    {
        return new static(
            $container->get('research_contracts.service'),
            $container->get('twig'),
        );
    }

    public function contracts(): ?array
    {
        $contracts = $this->contractsService->getContracts();
        $template = $this->twig->loadTemplate(
            $this->modulePath . '/templates/contracts.html.twig'
        );

        return [
            '#type' => 'markup',
            '#markup' => $template->render([
                'contracts' => $contracts,
            ]),
            '#attached' => [
                'library' => 'research_contracts/global',
            ]
        ];
    }
}
