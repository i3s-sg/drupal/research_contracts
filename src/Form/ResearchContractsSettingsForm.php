<?php

namespace Drupal\research_contracts\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;

class ResearchContractsSettingsForm extends ConfigFormBase
{

    public function getFormId()
    {
        return 'research_contracts_settings_form';
    }

    public function buildForm(array $form, FormStateInterface $form_state)
    {

        $form = parent::buildForm($form, $form_state);
        $config = $this->config('research_contracts.settings');

        $form['research_contracts_settings']['urls'] = [
            '#type' => 'details',
            '#title' => $this->t("Data Source URL"),
            '#open' => true,
        ];
        $form['research_contracts_settings']['urls']['contracts_url'] = [
            '#type' => 'textfield',
            '#title' => $this->t("Contracts data source:"),
            '#description' => $this->t("Full URL of the contracts data source."),
            '#default_value' => $config->get('research_contracts.contracts_url'),
            '#required' => true,
        ];

        $form['research_contracts_settings']['misc'] = [
            '#type' => 'details',
            '#title' => $this->t("Miscellaneous"),
            '#open' => false,
        ];
        $form['research_contracts_settings']['misc']['menu_weight'] = [
            '#type' => 'number',
            '#title' => $this->t("Menu item “weight”:"),
            '#description' => $this->t("Menu item display order (the lesser the leftier)."),
            '#default_value' => $config->get('research_contracts.menu_weight'),
            '#required' => true,
        ];

        return $form;
    }

    public function validateForm(array &$form, FormStateInterface $form_state)
    {
    }

    public function submitForm(array &$form, FormStateInterface $form_state)
    {
        $config = $this->config('research_contracts.settings');

        $config->set('research_contracts.contracts_url', trim($form_state->getValue('contracts_url')));
        $config->set('research_contracts.menu_weight', $form_state->getValue('menu_weight'));

        $config->save();

        \Drupal::service('router.builder')->rebuild();

        return parent::submitForm($form, $form_state);
    }

    protected function getEditableConfigNames()
    {
        return [
          'research_contracts.settings',
        ];
    }
}
