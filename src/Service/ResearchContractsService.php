<?php

namespace Drupal\research_contracts\Service;

use Drupal\i3s_commons\CurlCommons;

class ResearchContractsService
{
    private $curlCommons;
    private $researchContractsSettings;

    public function __construct()
    {
        $this->curlCommons = new CurlCommons();
        $this->researchContractsSettings = \Drupal::config('research_contracts.settings');
    }

    public function getContracts(): ?array
    {
        return $this->curlCommons->getData(
            $this->researchContractsSettings->get('research_contracts.contracts_url')
        );
    }
}
